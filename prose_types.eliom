[%%shared
  (* type text = string [@@deriving json]

  type cursor = {
    line : int;
    column : int;
  } [@@deriving json]

  type edit = {
    start : cursor;
    stop : cursor;
    text : text;
  } [@@deriving json]

  type add_edit = int * edit [@@deriving json] *)

  module Attributes = struct
    module Color = struct
      type rgb = { red : int; green : int; blue : int } [@@deriving json]
      type hsl = { hue : int; saturation : int; lightness : int } [@@deriving json]
      type hsv = { hue : int; saturation : int; value : int } [@@deriving json]
      type t =
        | RGB of rgb
        | HSL of hsl
        | HSV of hsv
        | None
      [@@deriving json]
      let export = function
        | None -> ""
        | RGB rgb ->
            Printf.sprintf "#%06x" (rgb.red * (256 * 256) + rgb.green * 256 + rgb.blue)
        | _ -> assert false
      let import = function
        | "none" -> None
        | o ->
            let i = Scanf.sscanf o "#%x" (fun i -> i) in
            RGB { red = i / (256 * 256); green = (i / 256) mod 256; blue = i mod 256 }
    end

    module Align = struct
      type t =
        | Center
        | Justify
        | Right
        | Left (* NB: I think this one doesn't actually exist: null is used instead *)
        | None
      [@@deriving json]
      let export = function
        | Center -> "center"
        | Justify -> "justify"
        | Right -> "right"
        | Left -> "left"
        | None -> "none"
      let import = function
        | "center" -> Center
        | "justify" -> Justify
        | "right" -> Right
        | "left" -> Left
        | "none" -> None
        | _ -> assert false
    end

    module Direction = struct
      type t =
        | RTL
        | LTR
        | None
      [@@deriving json]
      let export = function
        | RTL -> "rtl"
        | LTR -> "ltr"
        | None -> "none"
      let import = function
        | "rtl" -> RTL
        | "ltr" -> LTR
        | "none" -> None
        | _ -> assert false
    end

    module Lst = struct
      type t =
        | Ordered
        | Bullet
        | None
      [@@deriving json]
      let export = function
        | Ordered -> "ordered"
        | Bullet -> "bullet"
        | None -> "none"
      let import = function
        | "ordered" -> Ordered
        | "bullet" -> Bullet
        | "none" -> None
        | _ -> assert false
    end

    module Script = struct
      type t =
        | Sub
        | Super
        | None
      [@@deriving json]
      let export = function
        | Sub -> "sub"
        | Super -> "super"
        | None -> "none"
      let import = function
        | "sub" -> Sub
        | "super" -> Super
        | "none" -> None
        | _ -> assert false
    end

    module Size = struct
      type t =
        | Small
        | Normal
        | Large
        | Huge
      [@@deriving json]
      let export = function
        | Small -> "small"
        | Normal -> "normal"
        | Large -> "large"
        | Huge -> "huge"
      let import = function
        | "small" -> Small
        | "normal" -> Normal
        | "large" -> Large
        | "huge" -> Huge
        | _ -> assert false
    end

    type url = string [@@deriving json]

    type t = {
      align : Align.t option;
      background : Color.t option;
      blockquote : bool option;
      bold : bool option;
      (* code : string option; *)
      code_block : bool option;
      color : Color.t option;
      direction : Direction.t option;
      font : string option;
      header : int option;
      image : string option;
      indent : int option;
      italic : bool option;
      link : string option;
      list : Lst.t option;
      (* list/item *)
      script : Script.t option;
      size : Size.t option;
      strike : bool option;
      underline : bool option;
      video : string option;
    } [@@deriving json]

    let default = {
      align = None;
      background = None;
      blockquote = None;
      bold = None;
      (* code = None; *)
      code_block = None;
      color = None;
      direction = None;
      font = None;
      header = None;
      image = None;
      indent = None;
      italic = None;
      link = None;
      list = None;
      (* list/item *)
      script = None;
      size = None;
      strike = None;
      underline = None;
      video = None;
    } [@@deriving json]
  end

  module Insert = struct
    type text = {
      insert : int array;
      attributes : Attributes.t option;
    } [@@deriving json]

    (* type embed = {
      url : Attributes.url;
      link : Attributes.url option;
    } [@@deriving json] *)

    type t =
      | Text of text
      (* | Image of embed
      | Video of embed *)
    [@@deriving json]
  end

  module Delete = struct
    type t = {
      delete : int;
    } [@@deriving json]
  end

  module Retain = struct
    type t = {
      retain : int;
      attributes : Attributes.t option;
    } [@@deriving json]
  end

  type op =
    | Insert of Insert.t
    | Delete of Delete.t
    | Retain of Retain.t
  [@@deriving json]

  type delta = op list [@@deriving json]

  type author = string [@@deriving json]

  type down = author * delta [@@deriving json]

  type hello = author [@@deriving json]

  let may f o =
    match o with
    | Some o -> f o
    | None -> ()

  let may_map f o =
    match o with
    | Some o -> Some (f o)
    | None as o -> o

]

[%%server
  module Delta = struct
    module Attributes = struct
      let compose a1 a2 =
        let a1 = match a1 with Some a -> a | None -> Attributes.default in
        let a2 = match a2 with Some a -> a | None -> Attributes.default in
        let chose f1 f2 =
          if f2 <> None then f2 else f1
        in
        let a = Attributes.{
          align      = chose a1.align      a2.align;
          background = chose a1.background a2.background;
          blockquote = chose a1.blockquote a2.blockquote;
          bold       = chose a1.bold       a2.bold;
          (* code = chose a1.code a2.code; *)
          code_block = chose a1.code_block a2.code_block;
          color      = chose a1.color      a2.color;
          direction  = chose a1.direction  a2.direction;
          font       = chose a1.font       a2.font;
          header     = chose a1.header     a2.header;
          image      = chose a1.image      a2.image;
          indent     = chose a1.indent     a2.indent;
          italic     = chose a1.italic     a2.italic;
          link       = chose a1.link       a2.link;
          list       = chose a1.list       a2.list;
          (* list/item *)
          script     = chose a1.script     a2.script;
          size       = chose a1.size       a2.size;
          strike     = chose a1.strike     a2.strike;
          underline  = chose a1.underline  a2.underline;
          video      = chose a1.video      a2.video;
        }
        in
        let open Obj in
        let t = repr a in
        let only_nones = ref true in
        for i = 0 to size t - 1 do
          only_nones := !only_nones && obj (field t i) = None
        done;
        if !only_nones then
          None
        else
          Some a
    end

    module Op = struct
      type t = op
      let length = function
        | Insert (Insert.Text { Insert.insert }) -> Array.length insert
        | Delete { Delete.delete } -> delete
        | Retain { Retain.retain } -> retain
    end

    module Iterator = struct
      type u = {
        op : op;
        offset : int;
      }
      type t = u list

      let empty = []

      let make delta =
        delta
        |> List.rev_map (fun op -> { op; offset = 0 })
        |> List.rev

      let length = function
        | { op; offset } :: _ ->
            Op.length op - offset
        | [] ->
           0

      let next ?max_length = function
        | { op; offset } :: tl -> Some (
            let op_length_left = max (Op.length op - offset) 0 in
            let length, next =
              match max_length with
              | Some max_length when max_length < op_length_left ->
                  max_length, ({ op; offset = offset + max_length } :: tl)
              | Some _
              | None ->
                  (* quill's delta library resets its global offset here but
                   * we've constructed the list so that we have one offset per
                   * op and the value for every offset in tl is already 0 so we
                   * don't need to do anything *)
                  op_length_left, tl
            in
            let op = match op with
            | Delete d ->
                Delete { Delete.delete = length }
            | Retain r ->
                Retain { r with Retain.retain = length }
            | Insert (Insert.Text i) ->
                Insert.(Insert (Text { i with insert = Array.sub i.insert offset length }))
            in
            op, next
        )
        | [] -> None

    end

    type t = delta

    let empty = []

    let push t op =
      match List.rev t with
      | last :: tl ->
          let new_tl = match last, op with
          | Delete d1, Delete d2 ->
              let open Delete in
              Delete { delete = d1.delete + d2.delete } :: []
          | Insert (Insert.Text t1), Insert (Insert.Text t2) when Insert.(t1.attributes = t2.attributes) ->
              let open Insert in
              Insert (Text { t1 with insert = Array.append t1.insert t2.insert }) :: []
          | Retain r1, Retain r2 when Retain.(r1.attributes = r2.attributes) ->
              let open Retain in
              Retain { r1 with retain = r1.retain + r2.retain } :: []
          | (Delete _ as d), (Insert _ as i) ->
              d :: i :: []
          | _ ->
              last :: op :: []
          in
          List.rev (List.rev_append new_tl tl)
      | [] ->
          [ op ]

    let compose t1 t2 =
      let module I = Iterator in
      let rec aux t (it1, it2) =
        let max_length = min (I.length it1) (I.length it2) in
        match I.(next it1, next it2, next ~max_length it1, next ~max_length it2) with
        | None, None, _, _ ->
            t
        | _, Some (Insert _ as e, tl2), _, _ ->
            aux (push t e) (it1, tl2)
        | Some (Delete _ as e, tl1), _, _, _ ->
            aux (push t e) (tl1, it2)
        | Some (op1, tl1), None, _, _ ->
            aux (push t op1) (tl1, it2)
        | None, Some (op2, tl2), _, _ ->
            aux (push t op2) (it1, tl2)
        | _, _, Some (Retain r1, tl1), Some (Retain r2, tl2) ->
            let op = Retain {
              r1 with
              Retain.attributes =
                Attributes.compose r1.Retain.attributes r2.Retain.attributes;
            } in
            aux (push t op) (tl1, tl2)
        | _, _, Some (Insert (Insert.Text i1), tl1), Some (Retain r2, tl2) ->
            let op = Insert Insert.(Text {
              i1 with
              attributes =
                Attributes.compose i1.Insert.attributes r2.Retain.attributes;
            }) in
            aux (push t op) (tl1, tl2)
        | _, _, Some (Retain _, tl1), Some (Delete _ as op2, tl2) ->
            aux (push t op2) (tl1, tl2)
        | _, _, Some (Insert _, tl1), Some (Delete _, tl2) ->
            aux t (tl1, tl2)
        | Some (Insert _, _), _, (Some ((Delete _ | Retain _), _) | None), _
        (* | Some (Delete _, _), _, (Some ((Insert _ | Retain _), _) | None), _ *)
        | Some (Retain _, _), _, (Some ((Insert _ | Delete _), _) | None), _
        (* | _, Some (Insert _, _), _, (Some ((Delete _ | Retain _), _) | None) *)
        | _, Some (Delete _, _), _, (Some ((Insert _ (* | Retain _ *)), _) | None)
        | _, Some (Retain _, _), _, (Some ((Insert _ (* | Delete _ *)), _) | None)
        ->
          assert false
      in
      aux empty I.(make t1, make t2)

    (* XXX: this is purely client-side so we can keep the JS-only
     * implementation of quill/delta. *)
    let transform_position t i =
      (* We iterate over the op list from t in order to compute the new index
       * value i. A delete operation reduces the value of i, an insert one
       * increments it. The variable offset stores stores where in the document
       * we are and we stop when the offset is larger than i because the
       * corresponding operations will not change i anymore (they are later in
       * the document). *)
      let module I = Iterator in
      let rec aux it ~offset ~i =
        if offset <= i then
          match I.next it with
          | Some (Delete _ as op, it) ->
              let length = Op.length op in
              aux it
                ~offset:(offset + length)
                ~i:(i - (min length (i - offset)))
          | Some (Insert _ as op, it) ->
              let length = Op.length op in
              aux it
                ~offset:(offset + length)
                ~i:(i + (min length (i - offset)))
          | Some _ ->
              aux it
                ~offset
                ~i
          | None ->
              i
        else
          i
      in
      aux (I.make t) ~offset:0 ~i
  end
]

[%%client
  (* Conversion functions are defined on the client side only. It is not
   * possible to use the Js module server-side so we have to do everything on
   * the client. *)

  (* Javascript uses UTF-16 for its strings. We need to do some string
   * processing on the server too and therefore we can't handle strings as
   * opaque. We don't need to do much but we need to be able to get substrings
   * and concatenate strings. Fortunately, Quill and JS use indexes in the
   * UTF-16 stream without concerning themselves with visible glyphs or
   * anything: they just use it as a dumb array. We can therefore use an array
   * of 16 bits integers on the OCaml side and convert from and to JS very
   * easily.
   *
   * NOTE: I think this assumes UTF-16 little-endian. *)
  module StringAsArray = struct
    type t = int array (* each element can be 16 bits *)

    (* Convert a js_string into t. This creates an OCaml int array of the same
     * length as the JS string and each item is the charCode at the
     * corresponding index in the JS string. *)
    let of_js_string (s : Js.js_string Js.t) : t =
      (* The ##. operator below lets us access the length property of the JS
       * object. *)
      Array.init s##.length (fun i ->
        (* The ## operator below lets us call the charCodeAt method of the JS
         * object. It returns a float because out-of-bound accesses return NaN!
         * We cannot access the string outside of its bounds so we directly
         * convert the result to an int. *)
        truncate (s##charCodeAt i)
      )

    (* Convert a 16 bits integer to a JS string. Actually we want a "character"
     * but these don't exist in JS and strings of length 1 are used instead. *)
    let int16_to_js_string i =
      (* [Js.Unsafe.global##._String] returns the constructor from the String
       * object and we then use the [fromCharCode] static method to create a new
       * object populated with the char code [i]. *)
      Js.Unsafe.global##._String##.fromCharCode i

    (* Convert a t to a js_string.
     * This creates an empty js_string and iterates of the OCaml array to
     * concatenate each char code to it. *)
    let to_js_string (t : t) =
      let s = ref (Js.string "") in
      for i = 0 to Array.length t - 1 do
        s := (!s)##concat (int16_to_js_string t.(i))
      done;
      !s
  end

  module Delta = struct
    class type o = object
      method ops : int (* XXX: not actually int *) Js.t Js.js_array Js.t Js.readonly_prop
    end
    type t = o Js.t
    (* We need to create Delta objects but the "module" is not directly visible
     * because of webpack. Fortunately, quill offers an API to access its
     * dependencies and we can "import" the corresponding module to be able to
     * call "new Delta" later on. *)
    let delta_js = Js.Unsafe.(inject @@ eval_string "Quill.import('delta')")
    let empty = Js.Unsafe.new_obj delta_js [||]
  end

  module Document (* XXX : sig
    type o
    type t = o Js.t
    val empty : t
  end *) = struct
    class type o = object('self)
      inherit Delta.o
      method compose : Delta.t -> 'self Js.t Js.meth
    end
    type t = o Js.t
    let empty : t =
      Delta.empty
  end

  module Selection = struct
    class type o = object
      method index : int Js.readonly_prop
      method length : int Js.readonly_prop
    end
    type t = o Js.t
    let compose (t : t) (delta : Delta.t) =
      let djs = Js.Unsafe.new_obj Delta.delta_js [| Js.Unsafe.inject delta |] in
      let index = djs##transformPosition t##.index in
      let length = djs##transformPosition (t##.index + t##.length) - index in
      object%js
        val index = index
        val length = length
      end
  end

  module Quill = struct
    class type o = object
      method getSelection : unit -> Selection.t Js.opt Js.meth
      method setSelection : Selection.t -> unit Js.meth
      method setContents : Document.t -> unit Js.meth
      method on : Js.js_string Js.t -> (Delta.t -> 'a -> Js.js_string Js.t -> unit Lwt.t) Js.callback -> unit Js.meth
    end
    type t = o Js.t
    let t : (Js.Unsafe.any -> Js.Unsafe.any -> t) Js.constr = (* TODO: improve type *)
      (* Return the constructor for Quill objects, this will make it possible to
       * do "new Quill" from OCaml. Note the leading underscore. *)
      Js.Unsafe.global##._Quill
  end

  let delta_to_js =
    let add_attributes ~o ~attributes =
      match attributes with
      | None -> ()
      | Some a ->
          let open Attributes in
          let open Js in
          let string = function
            | "none" -> null
            | s -> Opt.return (string s)
          in
          let int = function
            | 0 -> null
            | i -> Opt.return i
          in
          let ajs = Js.Unsafe.coerce (object%js end) in
          may (fun x -> ajs##.align := string (Align.export x)) a.align;
          may (fun x -> ajs##.background := string (Color.export x)) a.background;
          may (fun x -> ajs##.blockquote := bool x) a.blockquote;
          may (fun x -> ajs##.bold := bool x) a.bold;
          (* may (fun x -> ajs##.code := x) a.code; *)
          may (fun x -> ajs##.code_block := bool x) a.code_block; (* XXX: '-' is problematic for the ##. syntax. *)
          may (fun x -> ajs##.color := string (Color.export x)) a.color;
          may (fun x -> ajs##.direction := string (Direction.export x)) a.direction;
          may (fun x -> ajs##.font := string x) a.font;
          may (fun x -> ajs##.header := int x) a.header;
          may (fun x -> ajs##.image := string x) a.image;
          may (fun x -> ajs##.indent := int x) a.indent;
          may (fun x -> ajs##.italic := bool x) a.italic;
          may (fun x -> ajs##.link := string x) a.link;
          may (fun x -> ajs##.list := string (Lst.export x)) a.list;
          may (fun x -> ajs##.script := string (Script.export x)) a.script;
          may (fun x -> ajs##.size := string (Size.export x)) a.size;
          may (fun x -> ajs##.strike := bool x) a.strike;
          may (fun x -> ajs##.underline := bool x) a.underline;
          may (fun x -> ajs##.video := string x) a.video;
          (Js.Unsafe.coerce o)##.attributes := ajs
    in

    let op = function
      | Insert Insert.Text { Insert.insert; attributes } ->
          let o = object%js
            val insert = StringAsArray.to_js_string insert
          end
          in
          add_attributes ~attributes ~o;
          Js.Unsafe.coerce o
      | Delete { Delete.delete } ->
          Js.Unsafe.coerce (object%js
            val delete = delete
          end)
      | Retain { Retain.retain; attributes } ->
          let o = object%js
            val retain = retain
          end
          in
          add_attributes ~attributes ~o;
          Js.Unsafe.coerce o
    in

    fun d ->
      object%js
        val ops =
          Array.of_list d
          |> Array.map op
          |> Js.array
      end

  let to_attributes a =
    let open Attributes in
    let open Js in
    let foo = function
      | Some x -> Some (Opt.get x (fun _ -> string "none"))
      | None -> None
    in
    let bar = function
      | Some x -> Some (Opt.get x (fun _ -> 0))
      | None -> None
    in
    let f g o =
      may_map g (Optdef.to_option o)
    in
    let f2 g o =
      may_map (fun x -> g (to_string x)) (foo (Optdef.to_option o))
    in
    let f3 o =
      bar (Optdef.to_option o)
    in
    {
      align = f2 Align.import a##.align;
      background = f2 Color.import a##.background;
      blockquote = f to_bool a##.blockquote;
      bold = f to_bool a##.bold;
      (* code = to_option a##.code; *)
      code_block = f to_bool a##.code_block;
      color = f2 Color.import a##.color;
      direction = f2 Direction.import a##.direction;
      font = f2 (fun x -> x) a##.font;
      header = f3 a##.header;
      image = f2 (fun x -> x) a##.image;
      indent = f3 a##.indent;
      italic = f to_bool a##.italic;
      link = f2 (fun x -> x) a##.link;
      list = f2 Lst.import a##.list;
      script = f2 Script.import a##.script;
      size = f2 Size.import a##.size;
      strike = f to_bool a##.strike;
      underline = f to_bool a##.underline;
      video = f2 (fun x -> x) a##.video;
    }

  let to_delta delta =
    let to_option = Js.Optdef.to_option in
    let js_bind f o =
      match to_option o with
      | Some o -> Some (f o)
      | None -> None
    in
    Js.to_array (Js.Unsafe.coerce delta)##.ops
    |> ArrayLabels.fold_left
      ~init:[]
      ~f:(fun acc op ->
        let op = Js.Unsafe.coerce op in
        let insert = to_option op##.insert in
        let retain = to_option op##.retain in
        let delete = to_option op##.delete in
        match insert, retain, delete with
        | Some insert, None, None ->
            let caml_op = Insert Insert.(Text {
              insert = StringAsArray.of_js_string insert;
              attributes = js_bind to_attributes op##.attributes;
            })
            in
            caml_op :: acc
        | None, Some retain, None ->
            let caml_op = Retain Retain.{
              retain = retain;
              attributes = js_bind to_attributes op##.attributes;
            }
            in
            caml_op :: acc
        | None, None, Some delete ->
            let caml_op = Delete Delete.{
              delete = delete;
            }
            in
            caml_op :: acc
        | _ ->
            assert false (* XXX *)
    )
    |> List.rev
]

[%%shared
  type client = {
    up : delta Eliom_react.Up.t;
    down : (author * delta) Eliom_react.Down.t;
    edit_event : delta React.event;
  }
]

[%%server
  type pad = {
    history : delta React.signal;
    client : client;
  }
]
