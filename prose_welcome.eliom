[%%server
open Prose_types

let recode ?nln ?encoding out_encoding
(* copied from Uutf's doc *)
    (src : [`Channel of in_channel | `String of string])
    (dst : [`Channel of out_channel | `Buffer of Buffer.t])
  =
  let rec loop d e = match Uutf.decode d with
  | `Uchar _ as u -> ignore (Uutf.encode e u); loop d e
  | `End -> ignore (Uutf.encode e `End)
  | `Malformed _ -> ignore (Uutf.encode e (`Uchar Uutf.u_rep)); loop d e
  | `Await -> assert false
  in
  let d = Uutf.decoder ?nln ?encoding src in
  let e = Uutf.encoder out_encoding dst in
  loop d e

let t ?a text =
  let b = Buffer.create (2 * String.length text) in
  recode ~encoding:`UTF_8 `UTF_16LE (`String text) (`Buffer b);
  let bs = Buffer.contents b in
  let text = Array.init (Buffer.length b / 2) (fun i -> Char.code bs.[2*i] + 256 * Char.code bs.[2*i + 1]) in
  Insert (Text { insert = text; attributes = a })

open Attributes

let title = { default with header = Some 1; align = Some Center }
let section = { default with header = Some 2 }
let bullet = { default with list = Some Lst.Bullet }
let color s = Some (Color.import s)

let message = [
  t ~a:title "Welcome to Prose!\n";

  t ~a:section "Collaborative Editing\n";
    t "This is a collaborative editing system based on Quill for the editing widget and Ocsigen for everything else.\n";
    t "Every change is propagated to every one else without latency and this makes it possible to write a document with many hands at the same time.\n";
    t "Jot notes down during a meeting, improve grammar and wording iteratively, add comments or extend sections. All of this can be done by different participants without blocking each other.\n";
    t "\n";

  t ~a:section "Rich-text Support\n";
    t "Rich-text means you are not limited to raw text. You can:\n";
      t ~a:bullet "Add bullet points,\n";
      t ~a:{ bullet with bold = Some true } "Write in bold,\n";
      t ~a:{ bullet with italic = Some true } "or in italics,\n";
      t ~a:{ bullet with size = Some Size.Large } "in various sizes,\n";
      t ~a:{ bullet with color = color "#4F8F4F" } "and colors,\n";
      t ~a:{ bullet with color = color "#2F6F2F"; background = color "#CFAFCF" } "including background ones,\n";
      t ~a:{ bullet with direction = Some RTL } "from right-to-left,\n";
      t ~a:{ bullet with script = Some Sub } "and";
      t ~a:{ bullet with script = Some None } " ";
      t ~a:{ bullet with script = Some Super } "more";
      t ~a:{ bullet with script = Some None } ".\n";
    t "\n";

  t ~a:section "Unicode and emojis\n";
    t "You can input anything you want, including emojis and combination of emojis.\n";
    t "Obviously, this includes the Unicode Snowman: ";
    t ~a:{ default with size = Some Size.Large } "⛄.\n";
    t "It also includes composed emojis: ";
    t ~a:{ default with size = Some Size.Large } "🏳️ + 🌈 = 🏳️‍🌈.\n";
    t "\n";

  t ~a:section "Free Software\n";
    t "Prose is free software: you can redistribute it and/or modify it under ";
    t "the terms of the GNU Affero General Public License as published by the ";
    t "Free Software Foundation, either version 3 of the License, or (at your ";
    t " option) any later version.\n";
    t "The project is developed on ";
    t ~a:{ default with link = Some "https://gitlab.com/adrien-n/prose" } "GitLab ";
    t "and its license can be read ";
    t ~a:{ default with link = Some "https://gitlab.com/adrien-n/prose/blob/master/LICENSE" } "online";
    t ".\n";
]

]
