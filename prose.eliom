(* Prose has a dumb client-server architecture. Each client has a local view
 * and model and sends changes done on the client back to the server which
 * merges and broadcasts them to all clients.
 *
 * The server is currently limited to two functions:
 * - relay edits to all other clients
 * - store, compose and send the result document to clients as they join
 *
 * Both client-side and server-side models are implemented through React
 * signals and events. Updates from and to clients are done through
 * Eliom_react.{Up,Down} values; in this context, "up" means it goes from the
 * client to the server and "down", the other way round.
 *
 * We first create an "up" channel which carries user changes to the server and
 * derive a React event from it.
 * Once we have a server-side React event for each client, we supplement it
 * with authorship information and create a "down" channel. We also fold over
 * its values in order to compose them and be able to feed new connectees with
 * the latest document immediately.
*)

[%%shared
  open Prose_types
]

[%%server
  let () =
    Random.self_init ()

  (* We need some client-specific data, at least to get the authorship right.
   * We will use authorship so that when a client is notified of its own
   * changes by the server, it knows it is already up-to-date (provided no
   * other client has sent changes in between of course).
   *
   * The Eliom way to be able to have per-client data is called "scopes". Scopes
   * make it possible to have functions and values evaluate differently
   * depending on the chosen scope type. Here we use the default "process scope"
   * which means the evaluations will be constant across a given user-side
   * process, i.e. a tab. *)
  let scope = Eliom_common.default_process_scope

  (* We create a random author name which is different for each client. This is
   * done through an eliom reference which evaluation will depend on the
   * client-side process being handled. *)
  let current_client =
    Eliom_reference.Volatile.eref_from_fun
      ~scope
      (fun () -> Printf.sprintf "author-%d" (Random.bits ()))

  (* We need to send data of type "delta" between from client to the server. We
   * simply create a service with the right which have been created by the JSON
   * deriving PPX when we've defined the types.
   *
   * However, we don't directly use the eliom parameter value and instead go
   * through the functions of the Eliom_react module in order to create a value
   * of kind "up". The client will then be able to use it to push data to the
   * server.
   *
   * We also pass ~scope:`Site so that the value is always site-wide: the scope
   * argument defaults to `Client_process when outside of initialization but we
   * want to reuse the result for all clients. *)
  let delta_up () =
    Eliom_parameter.ocaml "delta" [%derive.json: delta]
    |> Eliom_react.Up.create ~scope:`Site

  (* The server logic is implemented with Lwt_react code. As such, we need to
   * obtain Lwt_react values corresponding to the Ocsigen Eliom_react values.
   * Extraction is done with one of the [to_react] functions. *)
  let edit_event delta_up =
    Eliom_react.Up.to_react delta_up

  (* The first reason we need to obtain edits of each client is to broadcast
   * them to all others. We'll achieve this through an Eliom_react value of kind
   * "down".
   * Actually, along with each change, we also have to send the author. We'll
   * use the eliom reference that we've created above. Its value depends on the
   * client-side process which is currently being communicated with. We are
   * still processing the information that was sent by the author of the edit
   * and as such, the [current_client] reference will evaluate to the name of
   * the sender. *)
  let edit_down edit_event =
    edit_event
    |> Lwt_react.E.map (fun edit -> Eliom_reference.Volatile.get current_client, edit)
    |> Eliom_react.Down.of_react

  (* The histories for pads are stored behind an Eliom_reference that is
   * persistent. I guess there are calls to Marshal behind the scenes to
   * serialize the value as a string and store that directly in DB.
   * The histories are simply stored in a Hashtbl.t that maps a pad name to a
   * content and I've added a counter in order to be sure the value is always
   * updated in the database (I don't know if the system has "clever"
   * optimizations).
   * The global scope is used because we have no reason to be restrictive. *)
  let histories =
    Eliom_reference.eref
      ~scope:Eliom_common.global_scope
      ~persistent:"pads_v0"
      (0, Hashtbl.create 10)

  (* We also need to accumulate the changes in order to let clients that join
   * later on to catch up with everything that has been done so far.
   * Since we have a react event, we simply create a react signal by composing
   * each edit together.
   *
   * Moreover we also push each update in the history hashtable and update the
   * Eliom_reference so that the in-DB values are updated. Unfortunately all the
   * histories will be saved for each edit in any of the pads but that shouldn't
   * be an issue for now.
   * *)
  let history pad_name edit_event =
    let compose base delta =
      let document = Delta.compose base delta in
      let%lwt () = Eliom_reference.modify histories (fun (n, h) ->
        (* We don't only update the hashtable but need to expressly push an
         * update to the Eliom_reference. *)
        Hashtbl.replace h pad_name document;
        (n+1), h
      ) in
      Lwt.return document
    in
    (* Get the initial content for the given pad. Either we have some history
     * for it from a previous run, or we use the default welcome content. *)
    let%lwt beginning =
      let%lwt n, h = Eliom_reference.get histories in
      Lwt.return (match Hashtbl.find h pad_name with
      | pad -> pad
      | exception Not_found ->
        (* We only return the default value and don't put the pad in the
         * history yet. Otherwise we'd store the default value. *)
        Prose_welcome.message
      )
    in
    Lwt.return (Lwt_react.S.fold_s compose beginning edit_event)

  (* Put together the various bits from above. We return a value that is made of
   * two main components. First, the client data that will be used to
   * communicate with the clients; it must be used in [%%client ...] blocks and
   * must also be re-created each time the server is re-started. Then, the
   * history which is a server-only value and is persistent across restarts. *)
  let pad pad_name =
    let delta_up = delta_up () in
    let edit_event = edit_event delta_up in
    let edit_down = edit_down edit_event in
    let%lwt history = history pad_name edit_event in
    Lwt.return {
      client = {
        up = delta_up;
        down = edit_down;
        edit_event = edit_event;
      };
      history = history;
    }

  (* Store pads by name. We have to keep the communications channels with the
   * clients around and have a single set of values for each pad. *)
  let pad =
    let pads = Hashtbl.create 10 in
    fun pad_name ->
      match Hashtbl.find pads pad_name with
      | pad ->
          Lwt.return pad
      | exception Not_found ->
          let%lwt pad = pad pad_name in
          Hashtbl.add pads pad_name pad;
          Lwt.return pad
]

[%%client
  let with_backtrace f arg =
    try f arg with exn -> ignore (Lwt_log_js.fatal_f "exception: %s" (Printexc.to_string exn))

  (* We have all the tools to create the client-side elements now. The only data
   * we have to get from the server before doing so is the author name for the
   * current client and the current document state (i.e. history). *)
  let make ~down ~up ~author:current_author ~history () =

    (* This builds the JS Quill object and does basically nothing
     * ocsigen-specific. *)
    let quill = Prose_quill.setup () in

    (* Each client has its own view of what is the current document but once an
     * edit has been received from the server, we know that there is a consensus
     * on it and that it will never go away nor change. We simply create a react
     * signal to hold this in order to avoid doing useless re-computations. It
     * is used for logic inside Prose and changing it doesn't impact the web
     * page. *)
    let consensus, consensus_send = Lwt_react.S.create Document.empty in

    (* On top of the consensus document, each client has a set of local changes
     * that must be sent to the server and that might have to be re-applied on
     * top of whatever is the new document in case there have been concurrent
     * changes.
     * NOTE: this is currently done through a Queue of deltas so we can handle
     * changes in a FIFO manner without writing push or pop functions but
     * putting a mutable data structure inside a Lwt_react Signal makes little
     * sense actually; it is however possible to not pay attention to this fact
     * in the code below. *)
    let local_deltas, local_deltas_send = Lwt_react.S.create (Queue.create ()) in

    (* This is the callback that handles changes sent by the server. Each data
     * is the combination of author and change. *)
    let on_down ((author, delta) as down) =
      (* We extract both the current local changes and local consensus from the
       * react signals we've defined above. *)
      let local_deltas = Lwt_react.S.value local_deltas in
      let consensus = Lwt_react.S.value consensus in

      (* In order to use it with Quill, the change that we've received needs to
       * be translated to a JS object. *)
      let delta = delta_to_js delta in
      (* Since the server is already aware of this edit, we can move our
       * [consensus] document forward.
       * Note the use of the "##" syntax which is used to call methods of JS
       * objects. A similar syntax is used to access properties: "##." . *)
      let consensus = consensus##compose delta in
      (* We send the [consensus] document through Lwt_react in order to get it
       * for the next iteration of this callback (or for others). *)
      consensus_send consensus;

      (* So far we have only updated what we know is the most-recent value the
       * server and us agree on. We now need to update the view with it. If we
       * have local changes, i.e. changes that haven't been acknowledged by the
       * server yet, we need to replay them on top of the consensus.
       *
       * There is a simple but effective optimization to be done here. If the
       * change that has just been received comes from ourselves, we don't have
       * to do anything because we were already up-to-date: the only thing that
       * has changed is that we now know that the change has been received
       * server-side and that no other change has happened in parallel. *)
      if author = current_author then ignore (
        (* If this change comes from us in the first place, we simply remove it
         * from the list of changes that are still local. *)
        Queue.pop local_deltas
      )
      else ignore (
        (* If this change doesn't come from us, we will have to replay the
         * changes that are still local to us on top of the new consensus. We
         * also have to update the current user selection in the view (this
         * includes cursor position). *)
        (* The first step is to retrieve the selection from the view. *)
        let selection = quill##getSelection () in

        (* Once we've saved the selection, we replay our local edits on top of
         * the consensus and update the view with the result. *)
        quill##setContents (Queue.fold (fun c d -> c##compose d) consensus local_deltas);

        (* The getSelection() method above returns a null value if there was no
         * selection in the view (for instance because the view wasn't focused
         * in the page). As such, we use Js.Opt.map to only apply the
         * selection-update function if it wasn't null. The update of the
         * selection itself is fairly straight-forward and simply recomputes the
         * bounds of the selection (start position and length) and forwards them
         * to the view. *)
        Js.Opt.map selection (fun selection -> quill##setSelection (Selection.compose selection delta))
      )
    in

    (* We call the [on_down] function upon initialization to catch up with the
     * current document that the server has sent us. *)
    on_down ("history", history);

    (* We also need a callback for text-change events in order to store changes
     * and send them to the server.
     * Callbacks for Quill accept three arguments: [delta], [base] and
     * [source]. The first one is the change itself, the second is the document
     * on which it applies so that composing [delta] on top of [base] gives the
     * new document and the last one indicates what has caused the change: it
     * is often "user" or "api". *)
    let on_text_change delta base source =

      (* We only care about events done through user interaction currently .*)
      if Js.to_string source = "user" then
        (* Simply extract the current set of local changes, push the current one
         * at its end and update the react signal with the result. *)
        let local_deltas = Lwt_react.S.value local_deltas in
        Queue.push delta local_deltas;
        local_deltas_send local_deltas;

        (* We also send the value to the server but we first have to transform
         * because we have been using a JS object until now and the server won't
         * be able to handle that.
         * Once again, we communicate with the server through the ~% syntax
         * which lets us access the (server-side) [delta_up] sink and we simply
         * push data to it.
         * NOTE: it would be cleaner to instead "subscribe" to changes in the
         * [local_deltas] Lwt_react signal so we separate the propagation of
         * changes with their creation. *)
        up (to_delta delta)
      else
        (* If the event was not user-generated, we do nothing. *)
        Lwt.return ()
    in

    (* Now that the callback has been defined, we register it on the
     * "text-change" signal.
     * We use the [quill] JS object and its method [on]. However, this is
     * obviously a JS method and we need to convert its arguments to something
     * that JS will understand directly. The "text-change" string is converted
     * to a JS string (i.e. UTF-16) and the [on_text_change] callback is wrapped
     * through Js.wrap_callback. *)
    quill##on (Js.string "text-change") (Js.wrap_callback on_text_change);

    (* The last step is to receive and consume the edits broadcast by the server
     * through the ~%edit_down Eliom_react "down" value. *)
    Lwt_react.E.map (with_backtrace on_down) down
]

[%%server
  open Eliom_content.Html

  (* The function below creates the pages that are sent to clients. It is the
   * first place where we can do some things because code earlier was executed
   * in a global context while this will run in a per-client context.
   *
   * To be honest, I'm not sure what [user_id] is and what it can be used for.
   *
   * Overall, we do fairly little here besides putting everything together that
   * we've defined above. *)
  let main_service_handler user_id pad_name () =
    let%lwt pad = pad pad_name in

    (* We dereference [current_client] at a time where the context for the
     * current client is defined. Obviously, outside of any request, the scope
     * we are using is not defined because it is per client-side process. *)
    let current_client = Eliom_reference.Volatile.get current_client in
    (* Similarly, we take the current value of pad.history but we don't have to
     * think about scopes for it. *)
    let history = Lwt_react.S.value pad.history in

    (* Now that we have defined the initialization values on the server, we need
     * to send them to the client. This is done with the help of the [%client
     * ...] syntax in server-side blocks: it evaluates the corresponding code
     * on the client even though we are in a server block.
     * We only have to call the function we've defined for the initialization in
     * the client block above and pass it the values as references that it can
     * pull later one from the server through the ~% syntax or directly. Note
     * that if we didn't use ~% here, compilation would error out with an
     * "unbound value" error.
     * The syntax is slightly heavy and we have to specify the return type of
     * the computation with ": unit" because it can't be done automatically. *)
    [%client (ignore (make
      ~down:~%(pad.client.down)
      ~up:~%(pad.client.up)
      ~author:~%current_client
      ~history:~%history
      ()) : unit)
    ];

    (* This is how we ultimately build the page itself. We simply put two
     * elements in the central block:
       * quill.js
       * a div named "quill-editor" where the Quill client-side instance will be
       *   setup *)
    Prose_container.page
      ~head:[ Prose_quill.include_quill ]
      user_id
      [
        D.(div ~a:[ a_id "quill-editor" ] []);
      ]
]

(* Register the service under /pads. It takes a "pad=pad_name" parameter which
 * can also be provided by accessing /pads/pad_name. *)
let main_service =
  Eliom_service.create
    ~path:(Eliom_service.Path ["pads"])
    ~meth:(Eliom_service.Get Eliom_parameter.(suffix (string "pad")))
    ()

(* Put the "default" pad at the root so that / lands people on the same pad as
 * "/pads/default". *)
let main_service_handler_default user_id () () =
  main_service_handler user_id "default" ()

(* Last step: register this module so that incoming client requests can be
 * dispatched to it. *)
module Prose_app = Eliom_registration.App (struct
  let application_name = "prose"
  let global_data_path = None
end)

(* The code below has been generated by ocsigen-start. It registers all the
 * services with their respective handler.
 *
 * The let%server is a syntax extension to define server code. It is a shorthand to putting everything in a [%%server ...] block. *)
let%server () =
  Eliom_registration.Action.register
    ~service:Os_services.set_personal_data_service
    Prose_handlers.set_personal_data_handler;

  Eliom_registration.Redirection.register
    ~service:Os_services.set_password_service
    Prose_handlers.set_password_handler;

  Eliom_registration.Action.register
    ~service:Os_services.forgot_password_service
    Prose_handlers.forgot_password_handler;

  Eliom_registration.Action.register
    ~service:Os_services.preregister_service
    Prose_handlers.preregister_handler;

  Eliom_registration.Action.register
    ~service:Os_services.sign_up_service
    Os_handlers.sign_up_handler;

  Eliom_registration.Action.register
    ~service:Os_services.connect_service
    Os_handlers.connect_handler;

  Eliom_registration.Unit.register
    ~service:Os_services.disconnect_service
    (Os_handlers.disconnect_handler ~main_page:true);

  Eliom_registration.Any.register
    ~service:Os_services.action_link_service
    (Os_session.Opt.connected_fun
       Prose_handlers.action_link_handler);

  Eliom_registration.Action.register
    ~service:Os_services.add_email_service
    Os_handlers.add_email_handler;

  (* Register our handler for the [main_service] we've defined above. *)
  Prose_base.App.register
    ~service:main_service
    (Prose_page.Opt.connected_page main_service_handler);

  (* Also register a handle for the default [Os_services.main_service] service.
   * I haven't been able to just modify it so far and it takes no parameter so
   * instead I've decided to make it use default values, i.e. make it the same
   * as the pad named "default". *)
  Prose_base.App.register
    ~service:Os_services.main_service
    (Prose_page.Opt.connected_page main_service_handler_default);

  Prose_base.App.register
    ~service:Prose_services.about_service
    (Prose_page.Opt.connected_page Prose_handlers.about_handler);

  Prose_base.App.register
    ~service:Prose_services.settings_service
    (Prose_page.Opt.connected_page Prose_handlers.settings_handler)

let%server () =
  Eliom_registration.Ocaml.register
    ~service:Prose_services.upload_user_avatar_service
    (Os_session.connected_fun Prose_handlers.upload_user_avatar_handler)


(* Print more debugging information when <debugmode/> is in config file
   (DEBUG = yes in Makefile.options).
   Example of use:
   let section = Lwt_log.Section.make "Prose:sectionname"
   ...
   Lwt_log.ign_info ~section "This is an information";
   (or ign_debug, ign_warning, ign_error etc.)
 *)
let%server _ =
  if Eliom_config.get_debugmode ()
  then begin
    ignore
      [%client (
        (* Eliom_config.debug_timings := true; *)
        (* Lwt_log_core.add_rule "eliom:client*" Lwt_log.Debug; *)
        (* Lwt_log_core.add_rule "os*" Lwt_log.Debug; *)
        Lwt_log_core.add_rule "Prose*" Lwt_log.Debug
        (* Lwt_log_core.add_rule "*" Lwt_log.Debug *)
        : unit ) ];
    (* Lwt_log_core.add_rule "*" Lwt_log.Debug *)
    Lwt_log_core.add_rule "Prose*" Lwt_log.Debug
  end
