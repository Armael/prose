[%%server
  open Eliom_content.Html

  (* Some files that we serve are static and stored in the "static" directory
   * of the ocsigen server. Below we simply define a helper function to build
   * URIs for files in that directory. *)
  let static_uri = F.make_uri ~service:(Eliom_service.static_dir ())

  let include_quill =
    D.(script ~a:[a_src (static_uri ["js"; "quill.min.js" ])] (pcdata ""))
]

[%%client
  open Prose_types

  (* We need to create a JS Quill object. When creating it, we can pass
   * parameters through JSON (roughly). Which we'll do as a JS script that we'll
   * inject in the page. Note that the code will be parsed and checked at
   * compile-time. *)
  let setup () =
    let options = Js.Unsafe.(inject @@ js_expr
"
(function() {
var toolbarOptions = [
  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  ['blockquote', 'code-block'],

  [{ 'header': 1 }, { 'header': 2 }],               // custom button values
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
  [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
  [{ 'direction': 'rtl' }],                         // text direction

  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
  [{ 'font': [] }],
  [{ 'align': [] }],

  ['clean']                                         // remove formatting button
];

return {
  modules: {
    toolbar: toolbarOptions
  },
  placeholder: 'Write here',
  readOnly: false,
  theme: 'snow',
}
})()
")
		in

    let id = "quill-editor" in

    let elt = Js.Unsafe.(inject @@ js_expr ("document.getElementById('" ^ id ^ "')")) in
    (* The new%js syntax is a PPX which allows us to easily create JS objects as
     * if we used "new" directly in JS. *)
    new%js Quill.t elt options
]
